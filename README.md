Prizes Pool
===================

This is a sample project of subscribe users to win prizes based on the following rules.

##**Technical Specifications**
-------------

###**Dependencies:**

 * `ruby version 2.4.0`
 * `bundle`

### **To install gem dependencies:**
`bundle install`

### **Database dependencies:**
  * `postgresql`

### **Database initialization:**
`bundle exec rake db:create` will create the databse.
`bundle exec rake db:setup` will run the `migrations` and the `seed` data, it will create an admin user to Login into the admin page.


### **To run test stack:**
`bundle exec rspec`

It will run everything inside `spec` folder.

### **To run the project:**
`bundle exec rails s`


### **To access the project**
`http://localhost:3000` the subscriber page

`http://localhost:300/admin` to access the admin page to manage the `admin_users` and `prizes`.

There is a defaul `admin_user` created to login: `admin` and pass: `password`.