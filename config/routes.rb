Rails.application.routes.draw do

  namespace :admin do
    root to: 'sessions#new'
    post 'sessions/create' => 'sessions#create'
    resources :admin_users
    resources :prizes
  end

  root to: 'subscribers#new'
  resources :subscribers, only: [:new, :create, :show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
