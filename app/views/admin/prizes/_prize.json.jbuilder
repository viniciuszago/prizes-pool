json.extract! prize, :id, :prize, :stock, :created_at, :updated_at
json.url admin_prize_url(prize, format: :json)
