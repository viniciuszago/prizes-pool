json.extract! admin_user, :id, :username, :email, :password_digest, :created_at, :updated_at
json.url admin_admin_user_url(admin_user, format: :json)
