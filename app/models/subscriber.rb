class Subscriber < ApplicationRecord
  validates_presence_of :email, :subscribe_date
  validates :email, format:{ with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validate :already_registered_in_a_date

  before_validation :set_subscribe_date
  before_save :set_subscriber_number

  def set_subscribe_date
    self.subscribe_date = Time.now
  end

  def set_subscriber_number
    self.number = Subscriber.where(subscribe_date: (Date.today)..(Date.tomorrow)).count + 1
  end

  def already_registered_in_a_date
    unless Subscriber.where(email: self.email, subscribe_date: (Date.today)..(Date.tomorrow)).first.nil?
      errors.add(:email, "Already subscribed")
    end
  end
end
