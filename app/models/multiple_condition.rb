class MultipleCondition
  attr_accessor :condition

  def initialize(condition)
    @condition = condition
  end

  def match?(subscriber_number)
    return false if subscriber_number <= condition.after_subscribers
    condition.condition_values.any? { |value| subscriber_number >= value.to_i && subscriber_number % value.to_i == 0 }
  end
end
