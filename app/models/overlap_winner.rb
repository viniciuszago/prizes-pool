class OverlapWinner < ApplicationRecord
  belongs_to :prize_condition

  validates_presence_of :number, :date, :prize_condition
end
