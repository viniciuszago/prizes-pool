class Prize < ApplicationRecord
  validates_presence_of :prize, :stock

  has_many :prize_conditions, -> { where(active: true) }, dependent: :destroy
  accepts_nested_attributes_for :prize_conditions

  def prize_conditions_attributes=(attrs)
    attrs.values.map { |a|
      a["condition_values"] = a["condition_values"].split(" ")
    }
    super
  end
end
