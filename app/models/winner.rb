class Winner < ApplicationRecord
  belongs_to :subscriber
  belongs_to :prize_condition
end
