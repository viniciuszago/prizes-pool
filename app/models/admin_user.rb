class AdminUser < ApplicationRecord
	has_secure_password

	validates_uniqueness_of :username, :email
  validates :email, presence: true, format:{ with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates :password,  length: { minimum: 8 }
end
