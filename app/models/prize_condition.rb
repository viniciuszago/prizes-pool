class PrizeCondition < ApplicationRecord
  CONDITION_TYPES = {
    equal: EqualCondition,
    multiple: MultipleCondition
  }

  belongs_to :prize

  validates_presence_of :prize_id, :condition_type, :condition_values
end
