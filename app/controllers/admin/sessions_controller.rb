module Admin
  class SessionsController < ApplicationController
    before_action :logged_in_user

    def new
    end

    def create
      admin_user = AdminUser.where("email = ? OR username = ?", session_params[:login], session_params[:login]).first

      redirect_to admin_root_path, alert: 'Invalid login.' and return if admin_user.nil?

      if admin_user.authenticate(session_params[:password])
        session[:username] = admin_user.username
        redirect_to admin_admin_users_path, notice: "Welcome #{admin_user.username}."
      else
        redirect_to admin_root_path, alert: 'Invalid login.'
      end
    end

    private
      # Never trust parameters from the scary internet, only allow the white list through.
      def session_params
        params.require(:session_params).permit(:login, :password)
      end

      def logged_in_user
        redirect_to admin_admin_users_path if session[:username]
      end
  end
end
