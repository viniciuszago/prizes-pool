class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def authenticate!
  	redirect_to admin_root_path if session[:username].nil?
  end
end
