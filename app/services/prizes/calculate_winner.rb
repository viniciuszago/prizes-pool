module Prizes
  class CalculateWinner
    attr_accessor :subscriber

    def initialize(subscriber)
      @subscriber = subscriber
    end

    def winner
      conditions = get_conditions

      return false if conditions.empty?

      overlap_winner = get_overlap_winner(conditions.map(&:id))
      if overlap_winner
        winner = create_winner(overlap_winner.prize_condition)
        return winner
      end

      match_conditions = get_match_conditions(conditions)

      return false if match_conditions.empty?

      winner = create_winner(match_conditions.shift.condition)

      create_overlap_winners(match_conditions)

      winner
    rescue => error
      Rails.logger.error error.backtrace.join("\n")
      raise error
    end

    private

    def get_conditions
      PrizeCondition.joins(:prize).where(active: true).where("prizes.stock > 0")
    end

    def get_overlap_winner(condition_ids)
      OverlapWinner.find_by(number: subscriber_number, date: Date.today, prize_condition: condition_ids)
    end

    def get_match_conditions(conditions)
      conditions = conditions.map { |condition| PrizeCondition::CONDITION_TYPES.fetch(condition.condition_type.to_sym).new(condition) }

      conditions.select { |condition| condition.match?(subscriber_number) }
    end

    def create_winner(condition)
      winner = Winner.create!(subscriber: subscriber, win_date: Time.now, prize_condition: condition)
      prize = winner.prize_condition.prize
      prize.update_attributes(stock: (prize.stock - 1))
      winner
    end

    def create_overlap_winners(match_conditions)
      match_conditions.each_with_index do |match_condition, index|
        condition = match_condition.condition

        break if condition.prize.stock.zero?

        OverlapWinner.create!(number: subscriber.number + index + 1, date: Date.today, prize_condition: condition )
      end
    end

    def subscriber_number
      subscriber.number
    end
  end
end
