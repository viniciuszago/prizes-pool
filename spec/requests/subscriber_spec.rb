require 'rails_helper'

RSpec.describe "Subscribers", type: :request do
  describe "POST /subscribers" do
    let!(:subscriber_params) {
      { :subscriber => { :email => 'test@test.com' } }
    }

    context "when the subscriber is not yet subscribed in the date" do
      context "and the subscriber does not win the prize" do
        it "should notify the subscriber that it subscribed" do
          post '/subscribers', params: subscriber_params

          expect(response).to redirect_to(assigns(:subscriber))
          follow_redirect!

          expect(response).to render_template(:show)
          expect(response.body).to include("Subscribed")
        end
      end

      context "and the subscriber wins the prize" do
        let!(:prize_condition) {
          create(:prize_condition, condition_type: "multiple", condition_values: [1])
        }

        it "should notify the subscriber is subscribed and won the prize" do
          post '/subscribers', params: subscriber_params

          expect(response).to redirect_to(assigns(:subscriber))
          follow_redirect!

          expect(response).to render_template(:show)
          expect(response.body).to include("Congratulations")
        end
      end
    end

    context "when the subscriber is already subscribed in the date" do
      before do
        create(:subscriber, email: 'test@test.com')
      end

      it "should notify the subscriber that he is already subscribed" do
        post '/subscribers', params: subscriber_params

        expect(response).to render_template(:new)
        expect(assigns(:subscriber).errors).not_to be_nil
      end
    end

    context "without params" do
      it "should return bad request" do
        post '/subscribers', params: { :subscriber => { :email => '' } }

        expect(response).to render_template(:new)
        expect(assigns(:subscriber).errors).not_to be_nil
      end
    end

    context "with invalid email" do
      it "should return unprocessable entity" do
        post '/subscribers', params: { :subscriber => { :email => 'test' } }

        expect(response).to render_template(:new)
        expect(assigns(:subscriber).errors).not_to be_nil
      end
    end
  end
end
