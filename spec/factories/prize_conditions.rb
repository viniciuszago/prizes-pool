FactoryBot.define do
  factory :prize_condition do
    prize
    condition_type "equal"
    condition_values [1, 10, 20]
    active true

    trait :with_after_subscribers do
      after_sbuscribers 1000
    end
  end
end
