FactoryBot.define do
  factory :subscriber do
    sequence(:email) { |n| "test#{n}@test.com" }
    subscribe_date Time.now
  end
end
