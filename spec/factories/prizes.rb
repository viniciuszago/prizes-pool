FactoryBot.define do
  factory :prize do
    prize "100$"
    stock 1
    active true
  end
end
