require 'rails_helper'

RSpec.describe Admin::PrizesController, type: :controller do
  let(:valid_attributes) {
    {
      prize: '$100',
      stock: 10
    }
  }

  let(:invalid_attributes) {
    {
      prize: ''
    }
  }

  let(:valid_session) {
    { username: "test" }
  }

  describe "GET #index" do
    let!(:prize) {
      create(:prize)
    }

    it "returns a success response" do
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    let!(:prize) {
      create(:prize)
    }

    it "returns a success response" do
      get :show, params: {id: prize.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    let!(:prize) {
      create(:prize)
    }

    it "returns a success response" do
      get :edit, params: {id: prize.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Prize" do
        expect {
          post :create, params: {prize: valid_attributes}, session: valid_session
        }.to change(Prize, :count).by(1)
      end

      it "redirects to the created prize" do
        post :create, params: {prize: valid_attributes}, session: valid_session
        expect(response).to redirect_to(admin_prize_path(Prize.last))
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {prize: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    let!(:prize) {
      create(:prize)
    }

    context "with valid params" do
      let(:new_attributes) {
        {
          prize: "$200",
          stock: 5
        }
      }

      it "updates the requested prize" do
        put :update, params: {id: prize.to_param, prize: new_attributes}, session: valid_session
        prize.reload
        expect(prize.prize).to eq new_attributes[:prize]
        expect(response).to redirect_to(admin_prize_url(prize))
      end

      it "redirects to the prize" do
        put :update, params: {id: prize.to_param, prize: valid_attributes}, session: valid_session
        expect(response).to redirect_to(admin_prize_path(prize))
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put :update, params: {id: prize.to_param, prize: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    let!(:prize) {
      create(:prize)
    }

    it "destroys the requested prize" do
      delete :destroy, params: {id: prize.to_param}, session: valid_session
      expect(prize.reload.active).to eq(false)
    end

    it "redirects to the prizes list" do
      delete :destroy, params: {id: prize.to_param}, session: valid_session
      expect(response).to redirect_to(admin_prizes_url)
    end
  end

end
