require 'rails_helper'

RSpec.describe Admin::AdminUsersController, type: :controller do
  let(:valid_attributes) {
    {
      username: "test",
      email: "test@test.com",
      password: "test1234",
      password_confirmation: "test1234"
    }
  }

  let(:invalid_attributes) {
    {
      username: "test",
      email: "test"
    }
  }

  let(:valid_session) {
    { username: "test" }
  }

  describe "GET #index" do
    it "returns a success response" do
      admin_user = AdminUser.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #show" do
    it "returns a success response" do
      admin_user = AdminUser.create! valid_attributes
      get :show, params: {id: admin_user.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      admin_user = AdminUser.create! valid_attributes
      get :edit, params: {id: admin_user.to_param}, session: valid_session
      expect(response).to be_success
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new AdminUser" do
        expect {
          post :create, params: {admin_user: valid_attributes}, session: valid_session
        }.to change(AdminUser, :count).by(1)
      end

      it "redirects to the created admin_user" do
        post :create, params: {admin_user: valid_attributes}, session: valid_session
        expect(response).to redirect_to(admin_admin_user_url(AdminUser.last))
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {admin_user: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          username: "test2",
          password: valid_attributes[:password],
          password_confirmation: valid_attributes[:password]
        }
      }

      it "updates the requested admin_user" do
        admin_user = AdminUser.create! valid_attributes
        put :update, params: {id: admin_user.to_param, admin_user: new_attributes}, session: valid_session
        admin_user.reload
        expect(admin_user.username).to eq new_attributes[:username]
        expect(response).to redirect_to(admin_admin_user_url(admin_user))
      end

      it "redirects to the admin_user" do
        admin_user = AdminUser.create! valid_attributes
        put :update, params: {id: admin_user.to_param, admin_user: valid_attributes}, session: valid_session
        expect(response).to redirect_to(admin_admin_user_url(admin_user))
      end
    end

    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        admin_user = AdminUser.create! valid_attributes
        put :update, params: {id: admin_user.to_param, admin_user: invalid_attributes}, session: valid_session
        expect(response).to be_success
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested admin_user" do
      admin_user = AdminUser.create! valid_attributes
      expect {
        delete :destroy, params: {id: admin_user.to_param}, session: valid_session
      }.to change(AdminUser, :count).by(-1)
    end

    it "redirects to the admin_users list" do
      admin_user = AdminUser.create! valid_attributes
      delete :destroy, params: {id: admin_user.to_param}, session: valid_session
      expect(response).to redirect_to(admin_admin_users_url)
    end
  end

end
