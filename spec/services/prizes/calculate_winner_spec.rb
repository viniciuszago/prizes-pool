require 'rails_helper'

RSpec.describe Prizes::CalculateWinner do

  let!(:subscriber) { create(:subscriber) }
  subject { Prizes::CalculateWinner.new(subscriber) }

  describe "#process()" do
    context "when there are no prizes in the stock" do
      let!(:prize) {
        create(:prize, stock: 0)
      }

      it "should return winner equals to false" do
        expect(subject.winner).to be false
      end
    end

    context "when there is only one condition that match the subscriber to win the prize" do
      let!(:prize) {
        create(:prize)
      }

      context "given one subscriber" do
        context "and the subscriber does not match the prize condition" do
          let!(:prize_condition) {
            create(:prize_condition, condition_type: "equal", condition_values: [5], prize: prize)
          }

          it "should return winner equals to nil" do
            expect(subject.winner).to be false
          end
        end

        context "and the subscriber matchs the prize condition" do
          let!(:prize_condition) {
            create(:prize_condition, condition_type: "equal", condition_values: [1], prize: prize)
          }

          it "should return winner equals to true" do
            expect(subject.winner).not_to be false
          end

          it "should decrease prize stock in 1" do
            subject.winner
            expect(prize.reload.stock).to eq 0
          end
        end

        context "and the prize condition has an after subscriber condition" do
          context "and the after subscriber is higher than the subscriber number" do
            let!(:prize_condition) {
              create(:prize_condition, condition_type: "multiple", condition_values: [10], prize: prize, after_subscribers: 99)
            }

            it "should return winner equals to false" do
              allow(subscriber).to receive(:number).and_return(50)
              expect(subject.winner).to be false
            end
          end

          context "and the after subscriber is lower than the subscriber number" do
            let!(:prize_condition) {
              create(:prize_condition, condition_type: "multiple", condition_values: [10], prize: prize, after_subscribers: 99)
            }

            it "should return winner equals to false" do
              allow(subscriber).to receive(:number).and_return(100)
              expect(subject.winner).not_to be false
            end
          end
        end
      end

      context "given two subscribers" do
        let!(:another_subscriber) {
          create(:subscriber, email: "test2@test.com")
        }
        let!(:prize_condition) {
          create(:prize_condition, condition_type: "equal", condition_values: [1], prize: prize)
        }

        context "and the first subscriber matchs the prize condition" do
          it "should return winner equals to true for the first subscriber" do
            expect(subject.winner).not_to be false
          end

          it "should return winners equals to false for the second subscriber" do
            expect(Prizes::CalculateWinner.new(another_subscriber).winner).to be false
          end
        end
      end
    end

    context "when there is more than one condition that match the subscriber to win the prize" do
      let!(:prize) {
        create(:prize, stock: 10)
      }
      let!(:prize_condition_one) {
        create(:prize_condition, condition_type: "equal", condition_values: [10], prize: prize)
      }
      let!(:prize_condition_two) {
        create(:prize_condition, condition_type: "multiple", condition_values: [10], prize: prize)
      }

      before do
        subscriber.update_attributes(number: 10)
      end

      context "given two subscribers" do
        let!(:another_subscriber) {
          create(:subscriber)
        }

        before do
          allow(another_subscriber).to receive(:number).and_return(11)
        end

        context "and the first subscriber match the prize condition" do
          it "should return winner equals to true for the first subscriber" do
            expect(subject.winner).not_to be false
          end

          it "should move the condition of the next prize to the next subscriber" do
            expect { subject.winner }.to change(OverlapWinner, :count).by(1)

            overlap_winner = OverlapWinner.first
            expect(overlap_winner.number).to eq 11
            expect(overlap_winner.date).to eq Date.today
          end

          it "should return winners equals to true for the second subscriber" do
            subject.winner
            expect(Prizes::CalculateWinner.new(another_subscriber).winner).not_to be false
          end
        end
      end
    end
  end
end
