# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171126100700) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_users", force: :cascade do |t|
    t.string "username"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "overlap_winners", force: :cascade do |t|
    t.integer "number"
    t.date "date"
    t.bigint "prize_condition_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["prize_condition_id"], name: "index_overlap_winners_on_prize_condition_id"
  end

  create_table "prize_conditions", force: :cascade do |t|
    t.bigint "prize_id", null: false
    t.string "condition_type", null: false
    t.string "condition_values", null: false, array: true
    t.integer "after_subscribers", default: 0
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["prize_id"], name: "index_prize_conditions_on_prize_id"
  end

  create_table "prizes", force: :cascade do |t|
    t.string "prize", null: false
    t.integer "stock", default: 1, null: false
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subscribers", force: :cascade do |t|
    t.string "email", null: false
    t.datetime "subscribe_date", null: false
    t.integer "number", null: false
    t.index ["subscribe_date", "email"], name: "index_subscribers_on_subscribe_date_and_email", unique: true
    t.index ["subscribe_date"], name: "index_subscribers_on_subscribe_date"
  end

  create_table "winners", force: :cascade do |t|
    t.bigint "subscriber_id"
    t.bigint "prize_condition_id"
    t.datetime "win_date"
    t.index ["prize_condition_id"], name: "index_winners_on_prize_condition_id"
    t.index ["subscriber_id"], name: "index_winners_on_subscriber_id"
  end

  add_foreign_key "overlap_winners", "prize_conditions"
  add_foreign_key "prize_conditions", "prizes"
  add_foreign_key "winners", "prize_conditions"
  add_foreign_key "winners", "subscribers"
end
