class CreatePrizeConditions < ActiveRecord::Migration[5.1]
  def change
    create_table :prize_conditions do |t|
      t.references :prize, foreign_key: true, null: false
      t.string :condition_type, null: false
      t.string :condition_values, null: false, array: true
      t.integer :after_subscribers, default: 0
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
