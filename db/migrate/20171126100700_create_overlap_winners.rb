class CreateOverlapWinners < ActiveRecord::Migration[5.1]
  def change
    create_table :overlap_winners do |t|
      t.integer :number
      t.date :date
      t.references :prize_condition, foreign_key: true

      t.timestamps
    end
  end
end
