class CreateWinners < ActiveRecord::Migration[5.1]
  def change
    create_table :winners do |t|
      t.references :subscriber, foreign_key: true
      t.references :prize_condition, foreign_key: true
      t.datetime :win_date
    end
  end
end
