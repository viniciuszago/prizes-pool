class CreatePrizes < ActiveRecord::Migration[5.1]
  def change
    create_table :prizes do |t|
      t.string :prize, null: false
      t.integer :stock, null: false, default: 0
      t.boolean :active, defautl: true

      t.timestamps
    end
  end
end
