class CreateSubscribers < ActiveRecord::Migration[5.1]
  def change
    create_table :subscribers do |t|
      t.string :email, null: false
      t.datetime :subscribe_date, null: false
      t.integer :number, null: false
    end
    add_index :subscribers, :subscribe_date
    add_index :subscribers, [:subscribe_date, :email], unique: true
  end
end
